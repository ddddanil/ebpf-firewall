#![feature(ip_bits)]
use std::path::PathBuf;

use aya::maps::lpm_trie::Key;
use aya::maps::LpmTrie;
use aya::programs::{tc, SchedClassifier, TcAttachType};
use aya::{include_bytes_aligned, Ebpf};
use aya_log::EbpfLogger;
use clap::{Parser, ValueHint};
use color_eyre::eyre::eyre;
use color_eyre::Result as AnyResult;
use serde::Deserialize;
use tokio::signal;
use tracing::{debug, info, warn};
use tracing_subscriber::util::SubscriberInitExt;

#[derive(Debug, Parser)]
struct Opt {
    #[clap(short, long, default_value = "eth0")]
    iface: String,
    #[clap(short, long, value_hint = ValueHint::FilePath, default_value = "blocklist.toml")]
    blocklist: PathBuf,
}

#[derive(Debug, Deserialize)]
struct Blocklist {
    blocklist: Vec<cidr::Ipv4Cidr>,
}

fn load_ebpf_object() -> AnyResult<Ebpf> {
    // This will include your eBPF object file as raw bytes at compile-time and load it at
    // runtime. This approach is recommended for most real-world use cases. If you would
    // like to specify the eBPF program at runtime rather than at compile-time, you can
    // reach for `Bpf::load_file` instead.
    #[cfg(debug_assertions)]
    let mut bpf = Ebpf::load(include_bytes_aligned!(
        "../../target/bpfel-unknown-none/debug/ebpf-firewall"
    ))?;
    #[cfg(not(debug_assertions))]
    let mut bpf = Ebpf::load(include_bytes_aligned!(
        "../../target/bpfel-unknown-none/release/ebpf-firewall"
    ))?;
    if let Err(e) = EbpfLogger::init(&mut bpf) {
        // This can happen if you remove all log statements from your eBPF program.
        warn!("failed to initialize eBPF logger: {}", e);
    }
    Ok(bpf)
}

#[tokio::main]
async fn main() -> AnyResult<()> {
    init_environment()?;
    tracing_subscriber()?.init();

    let Opt { iface, blocklist } = Opt::parse();
    debug!("iface: {}", iface);

    debug!(blocklist_file = ?blocklist, "Blocklist file");
    let blocklist = tokio::fs::read_to_string(blocklist).await?;
    let Blocklist { blocklist } = toml::from_str::<Blocklist>(&blocklist)?;

    // error adding clsact to the interface if it is already added is harmless
    // the full cleanup can be done with 'sudo tc qdisc del dev eth0 clsact'.
    let _ = tc::qdisc_add_clsact(&iface);
    let mut bpf = load_ebpf_object()?;

    const IP4_BLOCKLIST_MAP: &'static str = "IP4_BLOCKLIST";
    let mut blocklist_map: LpmTrie<_, u32, u8> = LpmTrie::try_from(
        bpf.map_mut(IP4_BLOCKLIST_MAP)
            .ok_or(eyre!("missing map: {}", IP4_BLOCKLIST_MAP))?,
    )?;

    blocklist
        .into_iter()
        .try_for_each(|cidr| -> AnyResult<()> {
            let block_addr = cidr.first_address();
            let key = Key::new(cidr.network_length().into(), block_addr.to_bits());
            blocklist_map.insert(&key, 1, 0)?;
            Ok(())
        })?;

    const EBPF_PROGRAM_NAME: &'static str = "ebpf_firewall";
    let program: &mut SchedClassifier = bpf
        .program_mut(EBPF_PROGRAM_NAME)
        .ok_or(eyre!("missing program: {}", EBPF_PROGRAM_NAME))?
        .try_into()?;
    program.load()?;
    let link_id = program.attach(&iface, TcAttachType::Ingress)?;

    info!("Waiting for Ctrl-C...");
    signal::ctrl_c().await?;
    info!("Exiting...");

    program.detach(link_id)?;
    program.unload()?;

    Ok(())
}

fn init_environment() -> AnyResult<()> {
    color_eyre::install()?;

    // Bump the memlock rlimit. This is needed for older kernels that don't use the
    // new memcg based accounting, see https://lwn.net/Articles/837122/
    let rlim = libc::rlimit {
        rlim_cur: libc::RLIM_INFINITY,
        rlim_max: libc::RLIM_INFINITY,
    };
    let ret = unsafe { libc::setrlimit(libc::RLIMIT_MEMLOCK, &rlim) };
    if ret != 0 {
        debug!("remove limit on locked memory failed, ret is: {}", ret);
    };

    Ok(())
}

fn tracing_subscriber() -> AnyResult<impl tracing::Subscriber> {
    use tracing_error::ErrorLayer;
    use tracing_subscriber::fmt;
    use tracing_subscriber::prelude::*;
    use tracing_subscriber::EnvFilter;

    if std::env::var("RUST_LIB_BACKTRACE").is_err() {
        std::env::set_var("RUST_LIB_BACKTRACE", "1")
    }

    let subscriber = tracing_subscriber::Registry::default()
        .with(EnvFilter::try_from_default_env().unwrap_or_else(|_| "debug".into()))
        .with(
            fmt::layer()
                .with_level(true) // include levels in formatted output
                .with_target(true) // include targets
                .pretty(),
        )
        .with(ErrorLayer::default());

    Ok(subscriber)
}
