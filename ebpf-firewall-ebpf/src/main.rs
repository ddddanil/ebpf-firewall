#![no_std]
#![no_main]
#![feature(ip_bits)]

use core::{mem, net::Ipv4Addr};

use aya_ebpf::{
    bindings::{TC_ACT_PIPE, TC_ACT_SHOT},
    macros::{classifier, map},
    maps::{lpm_trie::Key, LpmTrie},
    programs::TcContext,
};
use aya_log_ebpf::{info, warn};
use network_types::{
    eth::{EthHdr, EtherType},
    ip::Ipv4Hdr,
};

#[map]
static IP4_BLOCKLIST: LpmTrie<u32, u8> = LpmTrie::with_max_entries(1024, 0);

fn is_ip4_blocked(ip4: Ipv4Addr) -> bool {
    let key = Key::new(32, ip4.to_bits());
    let entry = IP4_BLOCKLIST.get(&key);
    entry.is_some_and(|val| *val != 0)
}

enum Decision {
    Allow,
    Block,
}

impl Decision {
    pub fn from_is_blocked(is_blocked: bool) -> Decision {
        if is_blocked {
            Decision::Block
        } else {
            Decision::Allow
        }
    }
}

#[classifier]
pub fn ebpf_firewall(ctx: TcContext) -> i32 {
    match try_ebpf_firewall(ctx) {
        Ok(Decision::Allow) => TC_ACT_PIPE,
        Ok(Decision::Block) => TC_ACT_SHOT,
        Err(_) => TC_ACT_PIPE, // fail-safe
    }
}

fn check_ipv4_packet(ctx: &TcContext, ipv4hdr: &Ipv4Hdr) -> Decision {
    let dst_addr = Ipv4Addr::from_bits(u32::from_be(ipv4hdr.dst_addr));
    let decision = Decision::from_is_blocked(is_ip4_blocked(dst_addr));

    if matches!(decision, Decision::Block) {
        let octets = dst_addr.octets();
        info!(
            ctx,
            "blocked IPv4: {}.{}.{}.{}", octets[0], octets[1], octets[2], octets[3]
        );
    };
    decision
}

fn try_ebpf_firewall(ctx: TcContext) -> Result<Decision, ()> {
    let ethhdr: *const EthHdr = unsafe { ptr_at(&ctx, 0)? };

    let decision = match unsafe { *ethhdr }.ether_type {
        EtherType::Ipv4 => {
            let ipv4hdr: &Ipv4Hdr = unsafe { ptr_at::<Ipv4Hdr>(&ctx, EthHdr::LEN)?.as_ref() }
                .ok_or_else(|| warn!(&ctx, "ipv4hdr null"))?;

            {
                let src = Ipv4Addr::from_bits(u32::from_be(ipv4hdr.src_addr)).octets();
                let dst = Ipv4Addr::from_bits(u32::from_be(ipv4hdr.dst_addr)).octets();
                info!(
                    &ctx,
                    "IPv4 packet: SRC {}.{}.{}.{} DST {}.{}.{}.{}",
                    src[0],
                    src[1],
                    src[2],
                    src[3],
                    dst[0],
                    dst[1],
                    dst[2],
                    dst[3]
                );
            }

            check_ipv4_packet(&ctx, ipv4hdr)
        }
        _ => return Ok(Decision::Allow),
    };

    Ok(decision)
}

#[inline(always)]
unsafe fn ptr_at<T>(ctx: &TcContext, offset: usize) -> Result<*const T, ()> {
    let start = ctx.data();
    let end = ctx.data_end();
    let len = mem::size_of::<T>();

    if start + offset + len > end {
        return Err(());
    }

    Ok((start + offset) as *const T)
}

#[panic_handler]
fn panic(_info: &core::panic::PanicInfo) -> ! {
    unsafe { core::hint::unreachable_unchecked() }
}
